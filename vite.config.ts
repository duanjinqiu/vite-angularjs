import { defineConfig } from "vite";
import { resolve } from "node:path";
import babel from "@rollup/plugin-babel";

function pathResolve(src: string) {
  return resolve(__dirname, src);
}
export default defineConfig({
  base: "/dist/",
  plugins: [
    babel({
      babelHelpers: "bundled",
      exclude: "node_modules/**",
    }),
  ],
  build: {
    target: "es2015",
  },
});
