import angular from "angular";
const app = angular.module("app");
app.controller("indexCtrl", function IndexCtrl($scope, $state) {
  $scope.msg = "Hello World!";
  $scope.clickEvent = () => {
    $state.go("about");
  };
});
