import uiRouter from "angular-ui-router";
const app = angular.module("app", [uiRouter]);
angular
  .module("app")
  .config(function (
    $stateProvider,
    $urlRouterProvider,
    $controllerProvider,
    $compileProvider,
    $filterProvider,
    $provide
  ) {
    app.controller = $controllerProvider.register;
    app.directive = $compileProvider.directive;
    app.filter = $filterProvider.register;
    app.factory = $provide.factory;
    app.service = $provide.service;
    app.provider = $provide.provider;
    app.value = $provide.value;
    app.constant = $provide.constant;
    app.decorator = $provide.decorator;

    $stateProvider
      .state("index", {
        url: "/index",
        templateUrl: new URL("./index/index.html", import.meta.url).href,
        controller: "indexCtrl",
        resolve: {
          deps: [
            function () {
              return import("./index/index");
            },
          ],
        },
      })
      .state("about", {
        url: "/about",
        templateUrl: new URL("./about/index.html", import.meta.url).href,
        controller: "aboutCtrl",
        resolve: {
          deps: [
            function () {
              return import("./about/index");
            },
          ],
        },
      });
    $urlRouterProvider.otherwise("/index");
  });
